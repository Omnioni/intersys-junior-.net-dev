﻿using System;

namespace Task6
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] line1 = Console.ReadLine().Split(' ');
            string[] line2 = Console.ReadLine().Split(' ');

            if (line1.Length != line2.Length)
            {
                Console.WriteLine("NO");
            }
            else
            {
                string answer = checkPermutation(line1, line2) ? "YES" : "NO";
                Console.WriteLine(answer);
            }
            
        }

        public static bool checkPermutation(string[] tableA, string[] tableB)
        {
            Array.Sort(tableA);
            Array.Sort(tableB);

            for (int i = 0; i< tableA.Length; i++)
            {
                if (tableA[i] != tableB[i]) return false;
            }
            return true;
        }
    }
}
