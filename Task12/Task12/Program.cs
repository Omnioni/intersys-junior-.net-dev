﻿using System;

namespace Task12
{
    class Program
    {
        static void Main(string[] args)
        {
            int indexMaxValue = 0, maxValue = 0, currValue = 0;
            string[] line;

            Console.ReadLine();
            line = Console.ReadLine().Split(' ');

            for (int i = 0; i<line.Length; i++)
            {
                foreach(char c in line[i])
                {
                    currValue += c - '0';
                }
                if (currValue > maxValue)
                {
                    maxValue = currValue;
                    indexMaxValue = i;
                }
                currValue = 0;
            }

            Console.WriteLine(indexMaxValue);
        }
    }
}
