﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task8
{
    class Program
    {
        static void Main(string[] args)
        {
            int rows = 0, maxEndValue;
            List<int[]> inputData;
            StringBuilder answer = new StringBuilder();
            bool[] S;

            if (Int32.TryParse(Console.ReadLine(), out rows))
            {
                inputData = IterationByRows(rows);
                if (inputData != null)
                {
                    maxEndValue = findMaxEndValue(inputData);
                    S = findPrimes(maxEndValue);
                    foreach (int[] i in inputData)
                    {
                        answer.AppendLine($"{CountPrimes(i[0], i[1], S)}");

                    }
                    Console.WriteLine(answer.ToString());
                }
            }
            else
            {
                Console.WriteLine("Number of rows have to be an Integer");
            }
            
        }

        public static List<int[]> IterationByRows(int rows)
        {
            string[] line;
            int begin, end;
            List<int[]> inputData = new List<int[]>();

            for (int i = 0; i < rows; i++)
            {
                int[] interval = new int[2];
                line = Console.ReadLine().Split(" ");
                if (!Int32.TryParse(line[0], out begin))
                {
                    Console.WriteLine("Variable \"m\" have to be an Integer");
                    return null;
                }
                else
                {
                    interval[0] = begin;
                }
                if (!Int32.TryParse(line[1], out end  ))
                {
                    Console.WriteLine("Variable \"n\" have to be an Integer");
                    return null;
                }
                else
                {
                    interval[1] = end;
                }
                inputData.Add(interval);
                
            }

            return inputData;
           
        }

        public static int findMaxEndValue(List<int[]> list)
        {
            int max = 0;
            foreach (int[] i in list)
            {
                if (i[1] > max)
                {
                    max = i[1];
                }
            }
            return max;
        }

        public static bool[] findPrimes(int n)
        {
            int g, x, y, xx, yy, z, i;
            bool[] S;

            S = new bool[n + 1];
            for (i = 5; i <= n; i++) S[i] = false;
            g = (int)(Math.Sqrt(n));
            for (x = 1; x <= g; x++)
            {
                xx = x * x;
                for (y = 1; y <= g; y++)
                {
                    yy = y * y;
                    z = (xx << 2) + yy;
                    if ((z <= n) && ((z % 12 == 1) || (z % 12 == 5))) S[z] = !S[z];
                    z -= xx;
                    if ((z <= n) && (z % 12 == 7)) S[z] = !S[z];
                    if (x > y)
                    {
                        z -= yy << 1;
                        if ((z <= n) && (z % 12 == 11)) S[z] = !S[z];
                    }
                }
            }
            for (i = 5; i <= g; i++)
                if (S[i])
                {
                    xx = i * i;
                    z = xx;
                    while (z <= n)
                    {
                        S[z] = false;
                        z += xx;
                    }
                }

            return S;
        }

        public static int CountPrimes(int begin, int end, bool[] S)
        {
            int counter;

            switch (begin)
            {
                case 1:                    
                case 2:
                    counter = 2;
                    break;
                case 3:
                    counter = 1;
                    break;
                default:
                    counter = 0;
                    break;

            }


            
            for (int i = begin; i <= end; i++)
                if (S[i]) counter++;
            return counter;
        }

    }
}
