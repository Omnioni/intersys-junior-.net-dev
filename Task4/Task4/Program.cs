﻿using System;
using System.Text;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            int tableLength = 0;
            string tableInLine;

            if (Int32.TryParse(Console.ReadLine(), out tableLength))
            {
                tableInLine = Console.ReadLine();
                StringBuilder reverseTableInLine = new StringBuilder(tableLength);
                foreach (string node in tableInLine.Split(' '))
                {
                    reverseTableInLine.Insert(0, $"{node} ");
                }
                Console.WriteLine(reverseTableInLine.ToString());
            }
            else
            {
                Console.WriteLine("Length have to be an integer.");
            }
        }
    }
}
