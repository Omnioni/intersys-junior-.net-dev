﻿using System;
using System.Text.RegularExpressions;

namespace Task5
{
    class Program
    {
        static void Main(string[] args)
        {
            string wordFromInput = Console.ReadLine();
            wordFromInput = wordFromInput.ToLower();
            bool result;
            wordFromInput = PrepareWord(wordFromInput);

            result = isPalindrome(wordFromInput);
            
            Console.WriteLine(result ? "YES" : "NO");
        }

        static string PrepareWord(string str)
        {
            str = Regex.Replace(str, "[^a-zA-Z]", "");

            return str;
        }

        static bool isPalindrome(string str)
        {
            for (int i = 0, j = str.Length - 1; i < j; i++, j--)
            {
                if (str[i] != str[j])
                {
                    return false;
                }
            }
            return true;
        }
    }
}
