﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Task7
{
    class Program
    {
        static void Main(string[] args)
        {
            int numberOfCharacter;
            int currentNumber = 0;
            List<int> list = new List<int>();
            while ((numberOfCharacter = Console.Read()) > 0)
            {
                if (numberOfCharacter == 10)
                {
                    if (isPowerOfTwo(currentNumber) && !list.Exists(x => x==currentNumber))
                    {
                        list.Add(currentNumber);
                    }
                    currentNumber = 0;
                }
                else
                {
                    if (numberOfCharacter >='0' && numberOfCharacter <= '9') {
                        currentNumber = currentNumber * 10 + (numberOfCharacter - '0');
                    }
                }
            }

            if (list.Count == 0)
            {
                Console.WriteLine("NA");
            }
            else
            {
                list.Sort();
                StringBuilder result = new StringBuilder();
                foreach(int i in list)
                {
                    result.Append($"{i},");
                }
                Console.WriteLine($"{result.ToString().Substring(0,result.Length-1)}");
            }
            Console.Read();
        }

        public static bool isPowerOfTwo(int num)
        {
            int power = 0;
            int result = 0;
            while ((result = (int)Math.Pow(2,power++)) <= num)
            {
                if (result == num)
                {
                    return true;
                } else if(result > num)
                {
                    return false;
                }
            }
            return false;
        }
    }
}
