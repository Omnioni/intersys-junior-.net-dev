﻿using System;

namespace Task11
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ReadLine();
            string line = Console.ReadLine();

            int[] result = new int[10];
            int number;
            int maxCountIndex = 0;
            foreach(char c in line)
            {
                if (c >= '0' && c <= '9')
                {
                    number = c - '0';
                    result[number]++;
                }
            }

            for (int i = 0; i<10; i++)
            {
                if (result[i] >= result[maxCountIndex])
                {
                    maxCountIndex = i;
                }
            }

            Console.WriteLine(maxCountIndex);

        }
    }
}
